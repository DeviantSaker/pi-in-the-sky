import usb.core
import usb.util


class FindClass(object):
    def __init__(self, class_):
        self._class = class_

    def __call__(self, device):
        if device.bDeviceClass == self._class:
            return True

        for cfg in device:
            intf = usb.util.find_descriptor(cfg, bInterfaceClass=self._class)

            if intf is not None:
                return True

        return False
