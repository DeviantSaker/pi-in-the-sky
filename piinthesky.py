"""Main file for the image capture.

Run using: sudo python piinthesky.py
Press Ctrl-C to stop
"""
# !/usr/bin/env python3
#
# Image capture from the sky.
# ======================================================================

__author__ = "Claire & Richard Gurman"

# Standard library imports.
import configparser
import os
import sys
import time

# Third party imports.
import picamera
import psutil
import RPi.GPIO as GPIO


# Prepare functions.
def getfilename():
    filename = time.strftime("%Y%m%d_%H%M%S", time.gmtime())
    return filename


def main():
    # Read config file.
    config = configparser.ConfigParser()
    config.read("config.ini")

    # Use PiGlow to indicate activity if configured.
    USE_PIGLOW = config.getboolean("GPIO", "PIGLOW", fallback=False)
    if USE_PIGLOW:
        import piglow
        piglow.auto_update = True
        piglow.clear_on_exit = True
        piglow.red(64)

    # Pin variables.
    VID_GPIO = config.getint("GPIO", "VID_PIN", fallback=7)
    PIC_GPIO = config.getint("GPIO", "PIC_PIN", fallback=11)
    PWR_GPIO = config.getint("GPIO", "PWR_PIN", fallback=37)
    LED_GPIO = config.getint("GPIO", "LED_PIN", fallback=36)

    # Initial setup.
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(VID_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(PIC_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(PWR_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(LED_GPIO, GPIO.OUT, initial=GPIO.LOW)

    SAVE_LOCATION = config.get("CAMERA", "LOCATION", fallback="skycapture")

    VIDEO_VFLIP = config.getboolean("CAMERA", "VFLIP", fallback=False)
    VIDEO_FRAMERATE = config.getint("CAMERA", "FRAMERATE", fallback=60)
    VIDEO_RESOLUTION = (config.getint("CAMERA", "RES_X", fallback=1280), config.getint("CAMERA", "RES_Y", fallback=720))
    VIDEO_ROTATION = config.getint("CAMERA", "ROTATION", fallback=0)

    # Initialise camera.
    camera = picamera.PiCamera()
    camera.framerate = VIDEO_FRAMERATE
    camera.resolution = VIDEO_RESOLUTION
    camera.vflip = VIDEO_VFLIP
    camera.rotation = VIDEO_ROTATION

    # Pause for loading.
    time.sleep(30)
    if USE_PIGLOW:
        piglow.orange(64)
    GPIO.output(LED_GPIO, True)

    storage_mount = "/home/pi"
    partitions = psutil.disk_partitions()
    for partition in partitions:
        if partition.device == "/dev/sda1":
            storage_mount = partition.mountpoint
            print(storage_mount)
            break

    storage_location = os.path.join(storage_mount, SAVE_LOCATION)
    if not os.path.isdir(storage_location):
        os.makedirs(storage_location)

    picture_capture = False
    video_capture = False
    video_changed = False
    button_pressed = None
    shutdown_trigger = False
    try:
        while True:
            if GPIO.input(VID_GPIO):
                video_changed = True
                video_capture = not video_capture

            if GPIO.input(PIC_GPIO):
                picture_capture = not video_capture

            if video_capture:
                if video_changed:
                    camera.start_recording(os.path.join(storage_location, getfilename() + ".h264"))
                    if USE_PIGLOW:
                        piglow.blue(64)
                    video_changed = False
                    print("Recording started")
                    time.sleep(1)
                else:
                    camera.wait_recording()
            else:
                if video_changed:
                    camera.stop_recording()
                    if USE_PIGLOW:
                        piglow.blue(0)
                    video_changed = False
                    print("Recording stopped")
                    time.sleep(1)

            if picture_capture:
                picture_capture = False
                if USE_PIGLOW:
                    piglow.yellow(64)
                camera.capture(os.path.join(storage_location, getfilename() + ".jpg"), use_video_port=True)
                if USE_PIGLOW:
                    piglow.yellow(0)
                print("Image captured")
                time.sleep(0.5)

            if not GPIO.input(PWR_GPIO):
                if button_pressed is None:
                    time.sleep(1)
                    button_pressed = int(time.time()) + 5
                    print("Button Pressed")
                elif button_pressed is not None and button_pressed >= int(time.time()) and not shutdown_trigger:
                    shutdown_trigger = True
                    raise Exception("Shutdown")

            if button_pressed is not None and button_pressed < int(time.time()):
                button_pressed = None

    finally:
        if video_capture:
            camera.stop_recording()

        GPIO.cleanup()
        if shutdown_trigger:
            print("Shutdown Triggered")
            os.system('sudo shutdown now -h')


if __name__ == "__main__":
    sys.exit(main())
